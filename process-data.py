import pandas as pd
#Read the csv file downloaded from the kimono api
df= pd.read_csv('data/kimonoData.csv', encoding='utf-8', skiprows=1,names=['title', 'stars', 'review'])
#print df[u'stars']

#remove duplicate rows
df.drop_duplicates(inplace=True)

#remove reviews with 3 stars because the sentiment analysis will only include good/bad not neutral
df = df[df['stars'] != '3 of 5 stars']


df['full_content'] = df['title'] + '. ' + df['review']

def get_class(stars):
	score = int(stars[0])
	if score > 3:
		return 'Good'
	else:
		return 'Bad'

#convert the number of starts into good or bad
df['true_category'] = df['stars'].apply(get_class)

df = df[['full_content', 'true_category']]

#print out a histogram of the sentiment values
print df['true_category'].value_counts()

#output to a file that can be fed into MonkeyLearn
df.to_csv('data/kimonoData_MonkeyLearn.csv', header=False, index=False, encoding='utf-8')
print "Conversion complete"