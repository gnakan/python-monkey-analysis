#Overview
A python script that converts Kimono provided data into proper format for use by MonkeyLearn. MonkeyLearn is used for sentiment analysis on TripAdvisor provided reviews. Once the data is converted, import the csv into MonkeyLearn.

After the data is imported into your MonkeyLearn module, you can use the sandbox to test out the trained classifer. If all goes well, after entering a review, you should see a json response similar to the following:

```json
{
  "status_code": "200",
  "queries_left": 991,
  "result": [
    {
      "probability": 0.872,
      "label": "Good"
    }
  ],
  "consumed_queries": 1
}
```

In the example above, the classifier has analyzed the sentiment of the entered review as "Good" with a certainity of 87.2%.


#Technology Stack
* Python


#Libraries, Frameworks & API's
* Pandas - http://pandas.pydata.org/
* Kimono - http://kimonolabs.com used to generate an api from hotel reviews on TripAdvisor, which was then used to export the data into csv format
* MonkeyLearn - http://www.monkeylearn.com/ The machine learning engine

#Other Notes
Included in the data/ folder is an example kimonoData_MonkeyLearn.csv. This includes 91 total samples that got fed into the MonkeyLearn classifier.

I was then able to test out the results. One sandbox test was to see if did the proper sentiment analysis on a review that said “I hated everything”. It labeled it as “Good”. After some experimentation, it appears the engine had equated the word “everything” with a good review. This is very likely based upon the samples that were fed to it.

Also, Kimono does not have to be used for data gathering.


#References
* [Kimono Guest Blog Post](http://blog.kimonolabs.com/2014/12/17/guest-blog-sentiment-analysis-on-web-scraped-data-with-kimono-and-monkeylearn/)